import os
import sys
from random import randint
import random
import math
import time
from time import sleep
import pickle
import numpy as np
import pprint
import json
import argparse
import subprocess
import datetime


#---------------------- key parameters captured by the --input parameter -----------------------
NUPACK = None
DANGLES = None
structure = None
MATERIAL = None
template = None
TEMPERATURE = None
MAX_ITERATIONS = None
target_normalized_ensemble_defect = None
S = None
#---------------------- key parameters captured by the --input parameter -----------------------

# global parameters
TARGET_GC = 0.50
SEED_GC = 0.50

# global data structures
POOL = []
SEEDS = []
ELITS = []
REJECTED = ['']

C_design = 0
C_eval = 0


def action(input_file_name, json_parameters):

	global NUPACK 
	NUPACK = json_parameters['NUPACK']
	global DANGLES
	DANGLES = json_parameters['dangles']
	global structure
	structure = json_parameters['target_structure']
	global MATERIAL
	MATERIAL = json_parameters['energy_model']
	global template
	template = json_parameters['design_template']
	global TEMPERATURE
	TEMPERATURE = " -T %s" %(str(json_parameters['design_temperature']))
	global MAX_ITERATIONS
	MAX_ITERATIONS = int(json_parameters['max_iterations'])
	global target_normalized_ensemble_defect
	target_normalized_ensemble_defect = float(json_parameters['target_normalized_ensemble_defect'])
	global S
	S = json_parameters['seed']

	design_params = {
	'NUPACK': NUPACK,
	'DANGLES': DANGLES,
	'MATERIAL': MATERIAL,
	'STRUCTURE': structure,
	'TEMPLATE': template,
	'SEED': S,
	'TEMPERATURE': TEMPERATURE.split(' ')[2],
	'MAX_ITERATIONS': MAX_ITERATIONS,
	'TARGET_NORMALIZED_ENSEMBLE_DEFECT': target_normalized_ensemble_defect
	}


	if not os.path.exists('./temp'):
		os.makedirs('./temp')

	if TEMPERATURE != " -T 37" and MATERIAL == 'rna1999':
		print "\nThe energy model of rna1999 can only support 37.0 degrees Celcius."
		print "Re-run by either adjusting the temperature or using rna1995 as the energy model\n"
		return

	if '[' not in structure and '{' not in structure:
		print '\n------------------------------------------------------------------------------------'
		print 'The input target structure does not have a pseudoknot; calling NUPACK design ...\n'
		sequence,normalized_ensemble_defect,free_energy,gc_level = design_with_nupack()
		print "target_structure -> ", structure
		print "sequence -> ", sequence
		print "free energy -> ", free_energy
		print "sequence G/C content -> ", gc_level
		print "temperature -> ", TEMPERATURE
		print "material -> ", MATERIAL
		print '\n'

		out_put_dictionary = {
		'target_structure': structure,
		'designed_sequence': sequence,
		'free_energy': float(free_energy),
		'sequence_GC_content': gc_level,
		'design_temperature': float(TEMPERATURE.split(' ')[2]),
		'energy_model': MATERIAL,
		'max_iterations': MAX_ITERATIONS,
		'design_template': template,
		'seed_sequence': S,
		'dangles': DANGLES
		}

		out_put_file_name = './results/' + input_file_name.split('.')[0] + '_' + str(datetime.datetime.now()) + '.json'
		with open(out_put_file_name, 'w') as f:
			f.write(json.dumps(out_put_dictionary, indent=4, sort_keys=True))

	else:
		print "\n----------------------------"
		print "Input design parameters are:"
		print "----------------------------\n"
		pprint.pprint(design_params, indent=4)
		print "\n"
		if pseudoknot_recognized() is False:
			print "Pseudoknot can not be classified. Try using another structure. For instance: ...............[[[..((((]]]..))))...."
			return
		print "Initializing Enzymer . . .\n"

		best_solution_found, i, all_defect_records, all_m_records, C_design, C_eval = ensemble_defect_optimization_with_dynamic_mutation("ENZ00001.ss", 
			structure, template, SEED_GC, S, target_normalized_ensemble_defect)
		print '\n-------------------------------------------------------------------------------------------------------------'
		print 'target structure -> ', structure
		print 'sequence -> ', best_solution_found['sequence']
		print 'sequence normalized ensemble defect ->', best_solution_found['normalized_ensemble_defect']
		print 'free energy ->' , best_solution_found['free_energy']
		print 'sequence G/C content ->', get_gc_percent(best_solution_found['sequence'])
		print 'temperature ->' , TEMPERATURE
		print 'material ->' , MATERIAL
		print 'max iterations ->', MAX_ITERATIONS
		print 'used iterations ', i
		print 'C_design in seconds', C_design
		print 'C_eval in seconds', C_eval
		print 'C_design / C_eval ', float(C_design/C_eval)
		print '\n'

		out_put_dictionary = {
		'target_structure': structure,
		'designed_sequence': best_solution_found['sequence'],
		'sequence_normalized_ensemble_defect': best_solution_found['normalized_ensemble_defect'],
		'free_energy': float(best_solution_found['free_energy']),
		'sequence_GC_content': get_gc_percent(best_solution_found['sequence']),
		'design_temperature': float(TEMPERATURE.split(' ')[2]),
		'energy_model': MATERIAL,
		'max_iterations': MAX_ITERATIONS,
		'used_iterations': i,
		'C_design_in_seconds': C_design,
		'C_eval_in_seconds': C_eval,
		'C_design/C_eval': float(C_design/C_eval),
		'design_template': template,
		'seed_sequence': S,
		'dangles': DANGLES
		}

		out_put_file_name = './results/' + input_file_name.split('.')[0] + '_' + str(datetime.datetime.now()) + '.json'
		with open(out_put_file_name, 'w') as f:
			f.write(json.dumps(out_put_dictionary, indent=4, sort_keys=True))


# -- Perform optimizations on a single given target. Each optimization prodecure follows a specific method
def ensemble_defect_optimization_with_a_single_point_or_nested_pair_mutation(T, 
	constrain, SEED_GC, STOP=0.005):
	# pair is only nested
	#try:
	#	os.popen('rm ./temp/*.*')
	#except:
	#	print './temp/*.* does not exist'
	seed = generate_seed(T, SEED_GC, option='-pseudo')
	max_iterations = int(50*len(T)) + 1
	iterations = 0
	C_eval_begin = time.time()
	dict = evaluate_sequence_ensemble_defect_single_objective(T, seed, constrain)
	C_eval_end = time.time()
	C_eval = C_eval_end - C_eval_begin
	all_defect_records = []
	#all_records.append(dict['normalized_ensemble_defect'])
	C_design_begin = time.time()
	C_design = 0
	for i in xrange(0, max_iterations):
		iterations = iterations + 1
		BEGIN = time.time()
		new_sequence = dict['sequence']
		current_defect = dict['normalized_ensemble_defect']
		all_defect_records.append(dict['normalized_ensemble_defect'])
		if current_defect <= STOP:
			#print 'STOP CONDITION ', STOP, ' reached...'
			C_design_end = time.time()
			C_design = C_design_end - C_design_begin
			return dict, iterations, all_defect_records, C_design, C_eval
		temp_dict = mutate_pseudo_only_nested(dict, constrain)
		if temp_dict['normalized_ensemble_defect'] < dict['normalized_ensemble_defect']:
			dict = temp_dict
		#print 'iteration ', i, ', seq -> ', dict['sequence'], ' | normal_defect-> ', dict['normalized_ensemble_defect'], ' | free_energy-> ', ' | GC_percent-> ', dict['GC_percent']
		END = time.time()
		#print 'iteration time -> ', END - BEGIN, '\n'
		C_design_end = time.time()
		C_design = C_design_end - C_design_begin

	return dict, iterations, all_defect_records, C_design, C_eval


def ensemble_defect_optimization_with_a_single_point_or_pair_mutation(T,
 constrain, SEED_GC, STOP=0.005):
	# pair is either nested or non-nested (type of pairs are distinguished here)
	#try:
	#	os.popen('rm ./temp/*.*')
	#except:
	#	print './temp/*.* does not exist'
	seed = generate_seed(T, SEED_GC, option='-pseudo')
	max_iterations = int(8*len(T)) + 1
	iterations = 0
	C_eval_begin = time.time()
	dict = evaluate_sequence_ensemble_defect_single_objective(T, seed, constrain)
	C_eval_end = time.time()
	C_eval = C_eval_end - C_eval_begin
	all_defect_records = []
	C_design_begin = time.time()
	C_design = 0
	for i in xrange(0, max_iterations):
		iterations = iterations + 1
		BEGIN = time.time()
		new_sequence = dict['sequence']
		current_defect = dict['normalized_ensemble_defect']
		all_defect_records.append(dict['normalized_ensemble_defect'])
		if current_defect <= STOP:
			#print 'STOP CONDITION ', STOP, ' reached...'
			C_design_end = time.time()
			C_design = C_design_end - C_design_begin
			return dict, iterations, all_defect_records, C_design, C_eval
		temp_dict = mutate_pseudo(dict, constrain)
		if temp_dict['normalized_ensemble_defect'] < dict['normalized_ensemble_defect']:
			dict = temp_dict
		#print 'iteration ', i, ', seq -> ', dict['sequence'], ' | normal_defect-> ', dict['normalized_ensemble_defect'], ' | free_energy-> ', ' | GC_percent-> ', dict['GC_percent']
		END = time.time()
		#print 'iteration time -> ', END - BEGIN, '\n'
		C_design_end = time.time()
		C_design = C_design_end - C_design_begin

	return dict, iterations, all_defect_records, C_design, C_eval


def ensemble_defect_optimization_with_dynamic_mutation(id, T, motifs, SEED_GC, S, STOP=0.005):
	#try:
	#	os.popen('rm ./temp/*')
	#except:
	#	print './temp/* does not exist'
	

	all_defect_records = []
	all_m_records = []
	records = []
	#RANDOM_SEED_GC = random.randint(25,75)
	#print 'SEED GC ', RANDOM_SEED_GC
	#seed = generate_seed(T, RANDOM_SEED_GC, option='-pseudo')
	#seed = 'AAAUAAAAAAGGAGGAGGAAGGGGGUGCACAAGGACACGUGCGCCCC'
	#constrain = 'xxxxxxxxxxooooooooooooooooooooooooooooooooooxxx'
	if len(S) == 0:
		seed, constrain = init_seed_with_motifs(T, motifs, SEED_GC, '-pseudo')
	else:
		seed, constrain = init_seed_with_motifs(T, motifs, SEED_GC, '-pseudo')
		seed = S

	print 'seed -> ',seed, ' seed GC ->', get_gc_percent(seed)
	print motifs
	print constrain


	C_eval_begin = time.time()
	dict = evaluate_sequence_ensemble_defect_single_objective(id,T, seed, constrain)
	C_eval_end = time.time()
	C_eval = C_eval_end - C_eval_begin

	print 'seed defect ', dict['normalized_ensemble_defect']
	print 'C_eval ', C_eval

	best_solution_found = dict
	
	C_design_end = 0
	# sampling 6 sequences from the mutational landscape of the seed. the number of mutations per samlpe sequence is randomely chosen
	m_1 = random.randint(1, int(len(T) - int(math.floor(len(T)*0.66))))
	dict_1 = m_mutant_pseudo(id, dict, int(m_1), constrain)
	print dict_1['normalized_ensemble_defect']
	
	m_2 = random.randint(1, int(len(T) - int(math.floor(len(T)*0.66))))
	dict_2 = m_mutant_pseudo(id, dict, int(m_2), constrain)
	print dict_2['normalized_ensemble_defect']
	
	m_3 = random.randint(1, int(len(T) - int(math.floor(len(T)*0.66))))
	dict_3 = m_mutant_pseudo(id, dict, int(m_3), constrain)
	print dict_3['normalized_ensemble_defect']
	
	m_4 = random.randint(1, int(len(T) - int(math.floor(len(T)*0.66))))
	dict_4 = m_mutant_pseudo(id, dict, int(m_4), constrain)
	print dict_4['normalized_ensemble_defect']
	
	m_5 = random.randint(1, int(len(T) - int(math.floor(len(T)*0.66))))
	dict_5 = m_mutant_pseudo(id, dict, int(m_5), constrain)
	print dict_5['normalized_ensemble_defect']

	C_design_begin = time.time()	
	m_6 = random.randint(1, int(len(T) - int(math.floor(len(T)*0.66))))
	dict_6 = m_mutant_pseudo(id, dict, int(m_6), constrain)
	print dict_6['normalized_ensemble_defect']
	
	
	temp_list = []
	temp_list.append(dict)
	temp_list.append(dict_1)
	temp_list.append(dict_2)
	temp_list.append(dict_3)
	temp_list.append(dict_4)
	temp_list.append(dict_5)
	temp_list.append(dict_6)

	dict, pos = compare_dicts(temp_list)
	records.append(float(dict['normalized_ensemble_defect']))
	all_defect_records.append(dict['normalized_ensemble_defect'])

	if pos == 0:
		all_m_records.append(m_1)
	if pos == 1:
		all_m_records.append(m_2)
	if pos == 2:
		all_m_records.append(m_3)
	if pos == 3:
		all_m_records.append(m_4)
	if pos == 4:
		all_m_records.append(m_5)
	if pos == 5:
		all_m_records.append(m_6)


	#if len(T) < 50:
	#	max_iterations = 400
	#else:
	#	max_iterations = int(8*len(T)) + 1
	max_iterations = MAX_ITERATIONS

	for i in xrange(1, max_iterations):
		BEGIN = time.time()
		if is_stuck(records, dict['normalized_ensemble_defect']) == True:
			#print "re-initializing the seed"
			#seed = generate_seed(T, SEED_GC, option='-pseudo')
			#dict = evaluate_sequence_ensemble_defect_single_objective(T, seed, constrain)
			# choose m at random
			m_1 = random.randint(5, int(len(T) - int(math.floor(len(T)*0.66))))
			m_2 = random.randint(5, int(len(T) - int(math.floor(len(T)*0.66))))
			m_3 = random.randint(5, int(len(T) - int(math.floor(len(T)*0.66))))
			m_4 = random.randint(5, int(len(T) - int(math.floor(len(T)*0.66))))
			m_5 = random.randint(5, int(len(T) - int(math.floor(len(T)*0.66))))
			#m = int (len(T)*0.66)
			#print 'Increasing m to'
			dict_1 = m_mutant_pseudo(id, dict, int(m_1), constrain)
			dict_2 = m_mutant_pseudo(id, dict, int(m_2), constrain)
			dict_3 = m_mutant_pseudo(id, dict, int(m_3), constrain)
			dict_4 = m_mutant_pseudo(id, dict, int(m_4), constrain)
			dict_5 = m_mutant_pseudo(id, dict, int(m_5), constrain)
			temp_list = []
			temp_list.append(dict_1)
			temp_list.append(dict_2)
			temp_list.append(dict_3)
			temp_list.append(dict_4)
			temp_list.append(dict_5)

			records = []
			dict, pos = compare_dicts(temp_list)
			records.append(dict['normalized_ensemble_defect'])
			all_defect_records.append(dict['normalized_ensemble_defect'])
			if pos == 0:
				all_m_records.append(m_1)
			if pos == 1:
				all_m_records.append(m_2)
			if pos == 2:
				all_m_records.append(m_3)
			if pos == 3:
				all_m_records.append(m_4)
			if pos == 4:
				all_m_records.append(m_5)

		new_seq = dict['sequence']
		current_defect = dict['normalized_ensemble_defect']

		my_random = random.random()
		if current_defect >= 0.50:
			mu = float(current_defect * len(T) * my_random * 0.20)
		if current_defect < 0.50 and current_defect >=0.20:
			mu = float(current_defect * len(T) * my_random * 0.21)
		if current_defect < 0.20:
			mu = float(current_defect * len(T) * my_random * 0.22)

		sigma = float(mu / 3)
		m_star = np.random.normal(mu, sigma, 1)
		m = 1
		if m_star < mu:
			m = math.floor(m_star-1)
			rnd = random.randint(0,3)
			if rnd == 0:
				m = 1
			if rnd == 1:
				m = 2
		if m_star > mu:
			m = math.ceil(m_star+1)
			rnd = random.randint(0,3)
			if rnd == 0:
				m = 1
			if rnd == 1:
				m = 2
		if m <= 0.50:
			m = 1
		if m < 1 and m > 0.50:
			m = 2
		m = int(m)
		print 'iteration:', i,' defect:', current_defect, ' mutations:', m, ' time: ', time.strftime("%I:%M:%S")
		print 'sequence: ', dict['sequence']
		temp_dict = m_mutant_pseudo(id, dict, m, constrain)

		all_m_records.append(m)
		if temp_dict['normalized_ensemble_defect'] < best_solution_found['normalized_ensemble_defect']:
			best_solution_found = temp_dict
		#print 'defect of newly mutated sequence: ', temp_dict['normalized_ensemble_defect']
		if temp_dict['normalized_ensemble_defect'] < current_defect:
			dict = temp_dict
		if temp_dict['normalized_ensemble_defect'] <= STOP:
			C_design_end = time.time()
			C_design = C_design_end - C_design_begin
			records.append(dict['normalized_ensemble_defect'])
			all_defect_records.append(dict['normalized_ensemble_defect'])
			#print "\nStop condtion ", STOP, " reached!\n"
			# compute the free energy of the solution
			file = open('./temp/temp_energy.in', 'w')
			file.write(best_solution_found['sequence'])
			file.write('\n')
			file.write(structure)
			file.close()

			cmd = NUPACK + "/bin/energy -pseudo -dangles " + DANGLES + " -material " + MATERIAL + TEMPERATURE + " < ./temp/temp_energy.in > ./temp/temp_energy.out"
			os.popen(cmd)
			file = open('./temp/temp_energy.out','r')
			r = file.read().split()
			e = r[-1]
			best_solution_found['free_energy'] = e
			os.popen('rm ./temp/temp_energy.in ./temp/temp_energy.out')
			return dict, i, all_defect_records, all_m_records, C_design, C_eval

		records.append(dict['normalized_ensemble_defect'])
		all_defect_records.append(dict['normalized_ensemble_defect'])
	
	C_design_end = time.time()
	C_design = C_design_end - C_design_begin
	#print 'Stop condition NOT met !!'
	os.popen('cmd ./temp/ENZ00001.ss_nupack_prefix_temp.in ./temp/ENZ00001.ss_nupack_prefix_temp.ppairs')

	# compute the free energy of the solution
	file = open('./temp/temp_energy.in', 'w')
	file.write(best_solution_found['sequence'])
	file.write('\n')
	file.write(structure)
	file.close()

	cmd = NUPACK + "/bin/energy -pseudo -dangles " + DANGLES + " -material " + MATERIAL + TEMPERATURE + " < ./temp/temp_energy.in > ./temp/temp_energy.out"
	os.popen(cmd)
	file = open('./temp/temp_energy.out','r')
	r = file.read().split()
	e = r[-1]
	best_solution_found['free_energy'] = e
	os.popen('rm ./temp/temp_energy.in ./temp/temp_energy.out')

	return best_solution_found, i, all_defect_records, all_m_records, C_design, C_eval,


def ensemble_defect_optimization_with_dynamic_mutation_targetted(id, T, motifs, SEED_GC, STOP=0.005):
	#try:
	#	os.popen('rm ./temp/*')
	#except:
	#	print './temp/* does not exist'
	

	all_defect_records = []
	all_m_records = []
	records = []
	#RANDOM_SEED_GC = random.randint(25,75)
	#print 'SEED GC ', RANDOM_SEED_GC
	#seed = generate_seed(T, RANDOM_SEED_GC, option='-pseudo')
	#seed = 'AAAUAAAAAAGGAGGAGGAAGGGGGUGCACAAGGACACGUGCGCCCC'
	#constrain = 'xxxxxxxxxxooooooooooooooooooooooooooooooooooxxx'
	seed, constrain = init_seed_with_motifs(T, motifs, SEED_GC, '-pseudo')
	print motifs
	print constrain
	print seed

	C_eval_begin = time.time()
	dict = evaluate_sequence_ensemble_defect_single_objective(id,T, seed, constrain)
	C_eval_end = time.time()
	C_eval = C_eval_end - C_eval_begin

	print 'seed defect ', dict['normalized_ensemble_defect']
	print 'C_eval ', C_eval

	best_solution_found = dict
	
	C_design_end = 0
	# sampling 6 sequences from the mutational landscape of the seed. the number of mutations per samlpe sequence is randomely chosen
	m_1 = random.randint(1, int(len(T) - int(math.floor(len(T)*0.66))))
	dict_1 = m_mutant_pseudo(id, dict, int(m_1), constrain)
	print dict_1['normalized_ensemble_defect']
	
	m_2 = random.randint(1, int(len(T) - int(math.floor(len(T)*0.66))))
	dict_2 = m_mutant_pseudo(id, dict, int(m_2), constrain)
	print dict_2['normalized_ensemble_defect']
	
	m_3 = random.randint(1, int(len(T) - int(math.floor(len(T)*0.66))))
	dict_3 = m_mutant_pseudo(id, dict, int(m_3), constrain)
	print dict_3['normalized_ensemble_defect']
	
	m_4 = random.randint(1, int(len(T) - int(math.floor(len(T)*0.66))))
	dict_4 = m_mutant_pseudo(id, dict, int(m_4), constrain)
	print dict_4['normalized_ensemble_defect']
	
	m_5 = random.randint(1, int(len(T) - int(math.floor(len(T)*0.66))))
	dict_5 = m_mutant_pseudo(id, dict, int(m_5), constrain)
	print dict_5['normalized_ensemble_defect']

	C_design_begin = time.time()	
	m_6 = random.randint(1, int(len(T) - int(math.floor(len(T)*0.66))))
	dict_6 = m_mutant_pseudo(id, dict, int(m_6), constrain)
	print dict_6['normalized_ensemble_defect']
	
	
	temp_list = []
	temp_list.append(dict)
	temp_list.append(dict_1)
	temp_list.append(dict_2)
	temp_list.append(dict_3)
	temp_list.append(dict_4)
	temp_list.append(dict_5)
	temp_list.append(dict_6)

	dict, pos = compare_dicts(temp_list)
	records.append(float(dict['normalized_ensemble_defect']))
	all_defect_records.append(dict['normalized_ensemble_defect'])

	if pos == 0:
		all_m_records.append(m_1)
	if pos == 1:
		all_m_records.append(m_2)
	if pos == 2:
		all_m_records.append(m_3)
	if pos == 3:
		all_m_records.append(m_4)
	if pos == 4:
		all_m_records.append(m_5)
	if pos == 5:
		all_m_records.append(m_6)


	#max_iterations = int(10*len(T)) + 1
	max_iterations = MAX_ITERATIONS

	for i in xrange(1, max_iterations):
		BEGIN = time.time()
		if is_stuck(records, dict['normalized_ensemble_defect']) == True:
			#print "re-initializing the seed"
			#seed = generate_seed(T, SEED_GC, option='-pseudo')
			#dict = evaluate_sequence_ensemble_defect_single_objective(T, seed, constrain)
			# choose m at random
			m_1 = random.randint(5, int(len(T) - int(math.floor(len(T)*0.66))))
			m_2 = random.randint(5, int(len(T) - int(math.floor(len(T)*0.66))))
			m_3 = random.randint(5, int(len(T) - int(math.floor(len(T)*0.66))))
			m_4 = random.randint(5, int(len(T) - int(math.floor(len(T)*0.66))))
			m_5 = random.randint(5, int(len(T) - int(math.floor(len(T)*0.66))))
			#m = int (len(T)*0.66)
			#print 'Increasing m to'
			dict_1 = m_mutant_pseudo(id, dict, int(m_1), constrain)
			dict_2 = m_mutant_pseudo(id, dict, int(m_2), constrain)
			dict_3 = m_mutant_pseudo(id, dict, int(m_3), constrain)
			dict_4 = m_mutant_pseudo(id, dict, int(m_4), constrain)
			dict_5 = m_mutant_pseudo(id, dict, int(m_5), constrain)
			temp_list = []
			temp_list.append(dict_1)
			temp_list.append(dict_2)
			temp_list.append(dict_3)
			temp_list.append(dict_4)
			temp_list.append(dict_5)

			records = []
			dict, pos = compare_dicts(temp_list)
			records.append(dict['normalized_ensemble_defect'])
			all_defect_records.append(dict['normalized_ensemble_defect'])
			if pos == 0:
				all_m_records.append(m_1)
			if pos == 1:
				all_m_records.append(m_2)
			if pos == 2:
				all_m_records.append(m_3)
			if pos == 3:
				all_m_records.append(m_4)
			if pos == 4:
				all_m_records.append(m_5)

		new_seq = dict['sequence']
		current_defect = dict['normalized_ensemble_defect']

		my_random = random.random()
		if current_defect >= 0.50:
			mu = float(current_defect * len(T) * my_random * 0.80)
		if current_defect < 0.50 and current_defect >=0.20:
			mu = float(current_defect * len(T) * my_random * 0.90)
		if current_defect < 0.20:
			mu = float(current_defect * len(T) * my_random * 0.97)

		'''
		if mu > 5:
			mu = mu - 1
			if mu > 15:
				mu = mu - 3
				if mu > 25:
					mu = mu - 5
		'''

		#print 'MU ---> ', mu
		sigma = float(mu / 6)
		#print 'SIGMA --->', sigma
		m_star = np.random.normal(mu, sigma, 1)
		m = 1
		if m_star < mu:
			m = math.floor(m_star-1)
			rnd = random.randint(0,3)
			if rnd == 0:
				m = 1
			if rnd == 1:
				m = 2
		if m_star > mu:
			m = math.ceil(m_star+1)
			rnd = random.randint(0,3)
			if rnd == 0:
				m = 1
			if rnd == 1:
				m = 2
		if m <= 0.50:
			m = 1
		if m < 1 and m > 0.50:
			m = 2
		m = int(m)
		print 'iteration:', i,' defect:', current_defect, ' mu:', mu, ' sigma:', sigma, ' m:', m
		#print 'the m of the m-mutant is ---> ', m
		temp_dict = m_mutant_pseudo(id, dict, m, constrain)

		all_m_records.append(m)
		if temp_dict['normalized_ensemble_defect'] < best_solution_found['normalized_ensemble_defect'] and temp_dict['normalized_ensemble_defect'] > STOP + 0.03:
			best_solution_found = temp_dict
		#print 'defect of newly mutated sequence: ', temp_dict['normalized_ensemble_defect']
		if temp_dict['normalized_ensemble_defect'] < current_defect and temp_dict['normalized_ensemble_defect'] > STOP - 0.0175:
			dict = temp_dict
		if temp_dict['normalized_ensemble_defect'] <= STOP + 0.02 and temp_dict['normalized_ensemble_defect'] > STOP - 0.02:
			C_design_end = time.time()
			C_design = C_design_end - C_design_begin
			records.append(dict['normalized_ensemble_defect'])
			all_defect_records.append(dict['normalized_ensemble_defect'])
			#print "\nStop condtion ", STOP, " reached!\n"
			return dict, i, all_defect_records, all_m_records, C_design, C_eval

		records.append(dict['normalized_ensemble_defect'])
		all_defect_records.append(dict['normalized_ensemble_defect'])
		#print 'iteration ', i, ', seq -> ', dict['sequence'], '| normal_defect-> ', dict['normalized_ensemble_defect'], '| free_energy-> ', dict['free_energy'], '| GC_percent-> ', dict['GC_percent'], ' m -> ', m
		#END = time.time()
		#print 'iteration time -> ', END - BEGIN, '\n'
	
	C_design_end = time.time()
	C_design = C_design_end - C_design_begin
	#print 'Stop condition NOT met !!'
	return best_solution_found, i, all_defect_records, all_m_records, C_design, C_eval,


def ensemble_defect_optimization_with_fully_random_mutation(T, constrain, SEED_GC, STOP=0.005):
	# mutation is either single point , or a pair. no info regarding pair probablities is used
	#try:
	#	os.popen('rm ./temp/*.*')
	#except:
	#	print './temp/*.* does not exist'
	seed = generate_seed(T, SEED_GC, option='-pseudo')
	max_iterations = int(8*len(T)) + 1
	C_eval_begin = time.time()
	dict = evaluate_sequence_ensemble_defect_single_objective(T, seed, constrain)
	C_eval_end = time.time()
	C_eval = C_eval_end - C_eval_begin

	all_defect_records = []	
	#all_defect_records.append(dict['normalized_ensemble_defect'])
	iterations = 0
	C_design_begin = time.time()
	C_design = 0
	
	for i in xrange(0, max_iterations):
		iterations = iterations + 1
		BEGIN = time.time()
		new_sequence = dict['sequence']
		current_defect = dict['normalized_ensemble_defect']
		all_defect_records.append(dict['normalized_ensemble_defect'])
		if current_defect <= STOP:
			#print 'STOP CONDITION ', STOP, ' reached...'
			all_defect_records.append(dict['normalized_ensemble_defect'])
			C_design_end = time.time()
			C_design = C_design_end - C_design_begin
			return dict, iterations, all_defect_records, C_design, C_eval

		temp_dict = mutate_pseudo_random(dict, constrain)
		if temp_dict['normalized_ensemble_defect'] < dict['normalized_ensemble_defect']:
			dict = temp_dict
		
		#print 'iteration ', i, ', seq -> ', dict['sequence'], '| normal_defect-> ', dict['normalized_ensemble_defect'], '| free_energy-> ', dict['free_energy'], '| GC_percent-> ', dict['GC_percent']
		END = time.time()
		#print 'iteration time -> ', END - BEGIN, '\n'
		C_design_end = time.time()
		C_design = C_design_end - C_design_begin

	return dict, iterations, all_defect_records, C_design, C_eval


# -- Different types of mutation operators
def mutate_pseudo_random(dict, constrain):
	# perform a single point mutation with uniform probability
	# This mutation operator does not take constrains into consideration because it will never be used any way. 
	# It is just there to allow comparison between different mutation strategies

	new_sequence = None
	sequence = dict['sequence']
	structure = dict['structure']

	while(new_sequence == None or new_sequence == sequence):
		position = randint(1, len(sequence)-1)
		#print "position ", position, 'length of seq ', len(sequence)
		if structure[position] == '.':
			#print 'mutating a single nucletide'
			rnd = randint(0,3)
			if rnd == 0:
				new_sequence = sequence[0:position-1] + 'A' + sequence[position:]
			if rnd == 1:
				new_sequence = sequence[0:position-1] + 'G' + sequence[position:]
			if rnd == 2:
				new_sequence = sequence[0:position-1] + 'U' + sequence[position:]
			if rnd == 3:
				new_sequence = sequence[0:position-1] + 'C' + sequence[position:]

		if structure[position] == '(' or structure[position] == ')' or structure[position] == '[' or structure[position] == ']' or structure[position] == '{' or structure[position] == '}':
			j = get_complementary_bp_pseudo(structure, position+1)
			if position < j:
				#print 'mutating a pair'
				rnd = randint(0,5)
				#print rnd
				if rnd == 0:
					new_sequence = sequence[0:position-1] + 'A' + sequence[position:j-1] + 'U' + sequence[j:]
				if rnd == 1:
					new_sequence = sequence[0:position-1] + 'U' + sequence[position:j-1] + 'A' + sequence[j:]
				if rnd == 2:
					new_sequence = sequence[0:position-1] + 'G' + sequence[position:j-1] + 'C' + sequence[j:]
				if rnd == 3:
					new_sequence = sequence[0:position-1] + 'C' + sequence[position:j-1] + 'G' + sequence[j:]
				if rnd == 4:
					new_sequence = sequence[0:position-1] + 'G' + sequence[position:j-1] + 'U' + sequence[j:]
				if rnd == 5:
					new_sequence = sequence[0:position-1] + 'U' + sequence[position:j-1] + 'G' + sequence[j:]
			if j < position:
				rnd = randint(0,5)
				#print rnd
				if rnd == 0:
					new_sequence = sequence[0:j-1] + 'A' + sequence[j:position-1] + 'U' + sequence[position:]
				if rnd == 1:
					new_sequence = sequence[0:j-1] + 'U' + sequence[j:position-1] + 'A' + sequence[position:]
				if rnd == 2:
					new_sequence = sequence[0:j-1] + 'G' + sequence[j:position-1] + 'C' + sequence[position:]
				if rnd == 3:
					new_sequence = sequence[0:j-1] + 'C' + sequence[j:position-1] + 'G' + sequence[position:]
				if rnd == 4:
					new_sequence = sequence[0:j-1] + 'U' + sequence[j:position-1] + 'G' + sequence[position:]
				if rnd == 5:
					new_sequence = sequence[0:j-1] + 'G' + sequence[j:position-1] + 'U' + sequence[position:]

	#print len(structure), len(new_sequence), new_sequence
	dict = evaluate_sequence(structure, new_sequence, TARGET_GC, constrain, '-pseudo')
	return dict


def m_mutant_pseudo(id, dict, m, constrain):
	new_sequence = None
	sequence = dict['sequence']
	structure = dict['structure']
	original_seq = sequence

	while (hamming_distance(original_seq, new_sequence) < m or new_sequence == sequence or new_sequence == None):
		i = randint(1, len(sequence)) # randomly choose a position
		j = get_complementary_bp_pseudo(structure, i) # get the position of the complementary base pair of i. Note j might be None meaning i is free nt
		#print i, j

		if new_sequence != None:
			sequence = new_sequence

		if (m - hamming_distance(original_seq, sequence)) == 1:
			while j != None:
				i = randint(1, len(sequence))
				j = get_complementary_bp_pseudo(structure, i)

		current_hamming_distance = hamming_distance(sequence, new_sequence)

		if j == None:
			new_sequence = mutate_free_nucleotide(sequence, dict['pairs_matrix'], i, constrain)
			if new_sequence != None and new_sequence != sequence:
				new_hamming_distance = hamming_distance(sequence, new_sequence)
				if new_hamming_distance > current_hamming_distance:
					#dict = evaluate_sequence(structure, new_sequence, TARGET_GC, constrain, '-pseudo')
					constrain = update_constrain(constrain, i, None)
		
		if j != None:
			if i < j:
				new_sequence = mutate_pair(sequence, structure, dict['pairs_matrix'], dict['nested_pairs_matrix'], dict['non_nested_pairs_matrix'], i, j, constrain)
				new_hamming_distance = hamming_distance(sequence, new_sequence)
			
			if i > j:
				new_sequence = mutate_pair(sequence, structure, dict['pairs_matrix'], dict['nested_pairs_matrix'], dict['non_nested_pairs_matrix'], j, i, constrain)
				new_hamming_distance = hamming_distance(sequence, new_sequence)

			if new_sequence != None and new_sequence != sequence:
				if new_hamming_distance > current_hamming_distance:
					#dict = evaluate_sequence(structure, new_sequence, TARGET_GC, constrain, '-pseudo')
					constrain = update_constrain(constrain, i, j)
	
	dict = evaluate_sequence(id, structure, new_sequence, TARGET_GC, constrain, '-pseudo')
	return dict


def mutate_pseudo_only_nested(dict, constrain):
	# it first decides if it is going to go with a single position mutation or a pair mutation.
	# then, it will call the specialized mutation operator (i.e mutate_free_nucleotide vs. mutate_nested_pair)
	new_sequence = None
	sequence = dict['sequence']
	structure = dict['structure']

	while new_sequence == None or new_sequence == sequence:
		i = randint(1, len(sequence))
		j = get_complementary_bp_pseudo(structure, i)
		if j == None:
			#perform single point mutation
			new_sequence = mutate_free_nucleotide(sequence, dict['pairs_matrix'], i, constrain)
			if new_sequence != None and new_sequence != sequence:
				dict = evaluate_sequence_ensemble_defect_single_objective(structure, new_sequence, constrain)
				return dict
		if j != None:
			#perform basepair mutation considering only nested base pair probabilities.
			if i < j:
				new_sequence = mutate_nested_pair(sequence, structure, dict['pairs_matrix'], i, j, constrain)
			if i > j:
				new_sequence = mutate_nested_pair(sequence, structure, dict['pairs_matrix'], j, i, constrain)
			if new_sequence != None and new_sequence != sequence:
				dict = evaluate_sequence_ensemble_defect_single_objective(structure, new_sequence, constrain)
				return dict


def mutate_pseudo(dict, constrain):
	'''
	Decide if want to perform single point (or single pair) mutation or all positions could be subject to mutation
	by randomly choosing from either of case 1 or case 2.

	Probability for mutation for each single position or a paired position is described by:

		- Pm(base i free) = 1 - P(base i predicted to be free)
		- Pm(i and j form non-nested bp) = 1 - P(i and j predicted to form non-nested bp)
		- Pm(i and j form nested bp) = 1 - P(i and j predicted to form nested bp)
	
	case 1) single point (or single pair) mutation: keep selecting a single nucleotide OR a pair for mutation until 
			one single or one pair mutation occures. Each position (or pair) is subject to mutation according to 
			corresponding probability value.
	   
	case 2) limitless mutation: each position is subject to mutation accoring to corresponding probability values 

	Note: Always Check if the new sequence is not already rejected. If it is, then must try to re-mutate.

	Note: some positions must be able to remain locked (i.e. positional constrain on nucleotide content) and never 
	mutate as required by the user. This will allow insertion of known motifs inside the seed sequence.

	'''

	# This function (mutated_pseudo) quarantees the returned dict contains a brand new mutant.
	# however, it does not check whether the returned mutant has already been rejected or not

	new_sequence = None
	sequence = dict['sequence']
	structure = dict['structure']

	while (new_sequence == None or new_sequence == sequence):
		choice = random.random()
		#count = count + 1
		#print "COUNT = ", count
		# single mutation or many mutations?
		#print "Performing single point mutation"
		if choice <= 0.9999:
		# perform a a)single point or b)single pair mutation
			i = randint(1,len(sequence)) # randomly choose a position for mutation
			j = get_complementary_bp_pseudo(structure, i)

			# a) deal with single nucleotide mutation
			if j == None:
				#print "\nsingle nucleotide\n"
				new_sequence = mutate_free_nucleotide(sequence, dict['pairs_matrix'], i, constrain)
				#print "new sequence returned by single position mutation is ", new_sequence
				if new_sequence != None and new_sequence != sequence:
					#print "new sequence returned by single base mutation ", new_sequence
					dict = evaluate_sequence(structure, new_sequence, TARGET_GC, constrain, '-pseudo')
					return dict

			# b) deal with a base pair
			if j != None:
				#print "\nbase pair\n"
				
				if i < j:
					new_sequence = mutate_pair(sequence, structure, dict['pairs_matrix'], dict['nested_pairs_matrix'], dict['non_nested_pairs_matrix'], i, j, constrain)
				if i > j:
					new_sequence = mutate_pair(sequence, structure, dict['pairs_matrix'], dict['nested_pairs_matrix'], dict['non_nested_pairs_matrix'], j, i, constrain)

				if new_sequence != None and new_sequence != sequence:
					#print "new sequence returned by pair mutation ", new_sequence
					dict = evaluate_sequence(structure, new_sequence, TARGET_GC, constrain, '-pseudo')
					return dict

	# perform mutation on possibly many positions
		if choice > 0.9999 :
			#print "Performing multiple mutations"

		# For each alelle, apply a mutation; either a single base mutation or a pair mutation.
			index = 1
			for i in structure:
				if i == '.':
					#print 'free nt'
					new_sequence = mutate_free_nucleotide(sequence, dict['pairs_matrix'], index, constrain)
					if new_sequence != None:
						sequence = new_sequence
				if i == '(' or i == ')' or i == ']' or i == '[' or i == '{' or i == '}':
					#print 'pair nt'
					j = get_complementary_bp_pseudo(structure, index)
					#print 'i ', i, ' j ', structure[j-1], ' index ', index
					if index < j :
						new_sequence = mutate_pair(sequence, structure, dict['pairs_matrix'], dict['nested_pairs_matrix'], dict['non_nested_pairs_matrix'], index, j, constrain)
						if new_sequence != None:
							sequence = new_sequence
					if index > j:
						new_sequence = mutate_pair(sequence, structure, dict['pairs_matrix'], dict['nested_pairs_matrix'], dict['non_nested_pairs_matrix'], j, index, constrain)
						if new_sequence != None:
							sequence = new_sequence
				index = index + 1

		if new_sequence != None and sequence != new_sequence:
			dict = evaluate_sequence(structure, new_sequence, TARGET_GC, constrain, '-pseudo')
			sequence = dict['sequence']
			return dict


def mutate_nested_pair(sequence, structure, pairs_matrix, i, j, constrain):
	# this mutation operator treate all base pairs the same and uses the probability
	# of being [any] base pair to decide if it wants to mutate a position or not
	# the mutated sequence will not be re-evaluated here in this function
	# this function might return the same sequence
	new_sequence = None
	mutation = False
	prob_pair = pairs_matrix[i-1][j-1]
	prob_mutation = 1 - prob_pair
	decision = random.random()

	if (structure[i-1] == '(' or structure[i-1] == ')' or structure[i-1] == '[' or structure[i-1] == ']' or structure[i-1] == '{' or structure[i-1] == '}'):
		if (constrain[i-1] == 'x' and constrain[j-1] == 'x'):
			return None
		if (constrain[i-1] == 'x' and constrain[j-1] == 'o' and decision <= prob_mutation):
			if(sequence[i-1] == 'U'):
				rnd = random.randint(0,1)
				if rnd == 0:
					new_sequence = sequence[0:j-1] + 'G' + sequence[j:]
				if rnd == 1:
					new_sequence = sequence[0:j-1] + 'A' + sequence[j:]
			if(sequence[i-1] == 'G'):
				rnd = random.randint(0,1)
				if rnd == 0:
					new_sequence = sequence[0:j-1] + 'C' + sequence[j:]
				if rnd == 1:
					new_sequence = sequence[0:j-1] + 'U' + sequence[j:]
			if(sequence[i-1] == 'A'):
				new_sequence = sequence[0:j-1] + 'U' + sequence[j:]
			if(sequence[i-1] == 'C'):
				new_sequence = sequence[0:j-1] + 'G' + sequence[j:]
			return new_sequence

		if (constrain[i-1] == 'o' and constrain[j-1] == 'x' and decision <= prob_mutation):
			if(sequence[j-1] == 'U'):
				rnd = random.randint(0,1)
				if rnd == 0:
					new_sequence = sequence[0:i-1] + 'G' + sequence[i:]
				if rnd == 1:
					new_sequence = sequence[0:i-1] + 'A' + sequence[i:]
			if(sequence[j-1] == 'G'):
				rnd = random.randint(0,1)
				if rnd == 0:
					new_sequence = sequence[0:i-1] + 'C' + sequence[i:]
				if rnd == 1:
					new_sequence = sequence[0:j-1] + 'U' + sequence[i:]
			if(sequence[j-1] == 'A'):
				new_sequence = sequence[0:i-1] + 'U' + sequence[i:]
			if(sequence[j-1] == 'C'):
				new_sequence = sequence[0:i-1] + 'G' + sequence[i:]
			return new_sequence

		if (constrain[i-1] == 'o' and constrain[j-1] == 'o' and decision <= prob_mutation):
			rnd = random.randint(0,5)
			if rnd == 0:
				#print "A-U"
				new_sequence = sequence[0:i-1] + 'A' + sequence[i:j-1] + 'U' + sequence[j:]
				mutation = True
			if rnd == 1:
				#print "U-A"
				new_sequence = sequence[0:i-1] + 'U' + sequence[i:j-1] + 'A' + sequence[j:]
				mutation = True
			if rnd == 2:
				#print "G-U"
				new_sequence = sequence[0:i-1] + 'G' + sequence[i:j-1] + 'U' + sequence[j:]
				mutation = True
			if rnd == 3:
				#print "U-G"
				new_sequence = sequence[0:i-1] + 'U' + sequence[i:j-1] + 'G' + sequence[j:]
				mutation = True
			if rnd == 4:
				#print "G-C"
				new_sequence = sequence[0:i-1] + 'G' + sequence[i:j-1] + 'C' + sequence[j:]
				mutation = True
			if rnd == 5:
				#print "C-G"
				new_sequence = sequence[0:i-1] + 'C' + sequence[i:j-1] + 'G' + sequence[j:]
				mutation = True
			return new_sequence


def mutate_pair(sequence, structure, pairs_matrix, nested_pairs_matrix, non_nested_pairs_matrix,
 position_i, position_j, constrain):	
	# this mutation operator makes distinguish between a nested base pair and a non-nested base pair.
	# the mutated sequence will not be re-evaluated here in this function
	# this function might return the same sequence
	new_sequence = None
	mutation = False
	
	prob_nested_pair = nested_pairs_matrix[position_i-1][position_j-1]
	prob_non_nested_pair = non_nested_pairs_matrix[position_i-1][position_j-1]

	decision = random.random()

	prob_mutation_nested = 1 - prob_nested_pair
	prob_mutation_non_nested = 1 - prob_non_nested_pair
	
	# first need to decide if the pair is a nested pair or a non-nested pair
	# a) nested pair
	if (structure[position_i-1] == '(' or structure[position_i-1] == ')'):
		#deal with nested base pair
		#print 'mutation for nested base pair ', structure, position_i, position_j, structure[position_i-1], structure[position_j-1]

		if (constrain[position_i-1] == 'x' and constrain[position_j-1] == 'x'):
			#print 'xx'
			return None
		
		if (constrain[position_i-1] == 'x' and constrain[position_j-1] == 'o' and decision <= prob_mutation_nested):
			#print 'xo'
			if(sequence[position_i-1] == 'U'):
				rnd = random.randint(0,1)
				if rnd == 0:
					new_sequence = sequence[0:position_j-1] + 'G' + sequence[position_j:]
					mutation = True
				if rnd == 1:
					new_sequence = sequence[0:position_j-1] + 'A' + sequence[position_j:]
					mutation = True
			if(sequence[position_i-1] == 'G'):
				rnd = random.randint(0,1)
				if rnd == 0:
					new_sequence = sequence[0:position_j-1] + 'C' + sequence[position_j:]
					mutation = True
				if rnd == 1:
					new_sequence = sequence[0:position_j-1] + 'U' + sequence[position_j:]
					mutation = True
			if(sequence[position_i-1] == 'A'):
				new_sequence = sequence[0:position_j-1] + 'U' + sequence[position_j:]
				mutation = True
			if (sequence[position_i-1] == 'C'):
				new_sequence = sequence[0:position_j-1] + 'G' + sequence[position_j:]

			return new_sequence
		
		if (constrain[position_i-1] == 'o' and constrain[position_j-1] == 'x' and decision <= prob_mutation_nested):
			#print 'ox'
			if(sequence[position_j-1] == 'U'):
				rnd = random.randint(0,1)
				if rnd == 0:
					new_sequence = sequence[0:position_i-1] + 'G' + sequence[position_i:]
					mutation = True
				if rnd == 1:
					new_sequence = sequence[0:position_i-1] + 'A' + sequence[position_i:]
					mutation = True
			if(sequence[position_j-1] == 'G'):
				rnd = random.randint(0,1)
				if rnd == 0:
					new_sequence = sequence[0:position_i-1] + 'C' + sequence[position_i:]
					mutation = True
				if rnd == 1:
					new_sequence = sequence[0:position_i-1] + 'U' + sequence[position_i:]
					mutation = True
			if(sequence[position_j-1] == 'A'):
				new_sequence = sequence[0:position_i-1] + 'U' + sequence[position_i:]
				mutation = True
			if(sequence[position_j-1] == 'C'):
				new_sequence = sequence[0:position_i-1] + 'G' + sequence[position_i:]
				mutation = True
			return new_sequence
			
		if (constrain[position_i-1] == 'o' and constrain[position_j-1] == 'o' and decision <= prob_mutation_nested):
			#print 'oo'
			rnd = random.randint(0,5)
			if rnd == 0:
				#print "A-U"
				new_sequence = sequence[0:position_i-1] + 'A' + sequence[position_i:position_j-1] + 'U' + sequence[position_j:]
				mutation = True
			if rnd == 1:
				#print "U-A"
				new_sequence = sequence[0:position_i-1] + 'U' + sequence[position_i:position_j-1] + 'A' + sequence[position_j:]
				mutation = True
			if rnd == 2:
				#print "G-U"
				new_sequence = sequence[0:position_i-1] + 'G' + sequence[position_i:position_j-1] + 'U' + sequence[position_j:]
				mutation = True
			if rnd == 3:
				#print "U-G"
				new_sequence = sequence[0:position_i-1] + 'U' + sequence[position_i:position_j-1] + 'G' + sequence[position_j:]
				mutation = True
			if rnd == 4:
				#print "G-C"
				new_sequence = sequence[0:position_i-1] + 'G' + sequence[position_i:position_j-1] + 'C' + sequence[position_j:]
				mutation = True
			if rnd == 5:
				#print "C-G"
				new_sequence = sequence[0:position_i-1] + 'C' + sequence[position_i:position_j-1] + 'G' + sequence[position_j:]
				mutation = True
			return new_sequence

	
	# b) non-nested base pair
	if (structure[position_i-1] == '[' or structure[position_i-1] == ']' or structure[position_i-1] == '{' or structure[position_i-1] == '}'):
		#print 'mutation for non-nested base pair ', structure, position_i, position_j, structure[position_i-1], structure[position_j-1]

		if (constrain[position_i-1] == 'x' and constrain[position_j-1] == 'x'):
			return None

		if (constrain[position_i-1] == 'x' and constrain[position_j-1] == 'o' and decision <= prob_mutation_non_nested):
			if(sequence[position_i-1] == 'U'):
				rnd = random.randint(0,1)
				if rnd == 0:
					new_sequence = sequence[0:position_j-1] + 'G' + sequence[position_j:]
					mutation = True
				if rnd == 1:
					new_sequence = sequence[0:position_j-1] + 'A' + sequence[position_j:]
					mutation = True
			if(sequence[position_i-1] == 'G'):
				rnd = random.randint(0,1)
				if rnd == 0:
					new_sequence = sequence[0:position_j-1] + 'C' + sequence[position_j:]
					mutation = True
				if rnd == 1:
					new_sequence = sequence[0:position_j-1] + 'U' + sequence[position_j:]
					mutation = True
			if(sequence[position_i-1] == 'A'):
				new_sequence = sequence[0:position_j-1] + 'U' + sequence[position_j:]
				mutation = True
			if(sequence[position_i-1] == 'C'):
				new_sequence = sequence[0:position_j-1] + 'G' + sequence[position_j:]
				mutation = True
			return new_sequence

		if (constrain[position_i-1] == 'o' and constrain[position_j-1] == 'x' and decision <= prob_mutation_non_nested):
			if(sequence[position_j-1] == 'U'):
				rnd = random.randint(0,1)
				if rnd == 0:
					new_sequence = sequence[0:position_i-1] + 'G' + sequence[position_i:]
					mutation = True
				if rnd == 1:
					new_sequence = sequence[0:position_i-1] + 'A' + sequence[position_i:]
					mutation = True
			if(sequence[position_j-1] == 'G'):
				rnd = random.randint(0,1)
				if rnd == 0:
					new_sequence = sequence[0:position_i-1] + 'C' + sequence[position_i:]
					mutation = True
				if rnd == 1:
					new_sequence = sequence[0:position_i-1] + 'U' + sequence[position_i:]
					mutation = True
			if(sequence[position_j-1] == 'A'):
				new_sequence = sequence[0:position_i-1] + 'U' + sequence[position_i:]
				mutation = True
			if(sequence[position_j-1] == 'C'):
				new_sequence = sequence[0:position_i-1] + 'G' + sequence[position_i:]
				mutation = True
			return new_sequence

		if (constrain[position_i-1] == 'o' and constrain[position_j-1] == 'o' and decision <= prob_mutation_non_nested):
			rnd = random.randint(0,5)
			if rnd == 0:
				#A-U
				new_sequence = sequence[0:position_i-1] + 'A' + sequence[position_i:position_j-1] + 'U' + sequence[position_j:]
				mutation = True
			if rnd == 1:
				#U-A
				new_sequence = sequence[0:position_i-1] + 'U' + sequence[position_i:position_j-1] + 'A' + sequence[position_j:]
				mutation = True
			if rnd == 2:
				#G-U
				new_sequence = sequence[0:position_i-1] + 'G' + sequence[position_i:position_j-1] + 'U' + sequence[position_j:]
				mutation = True
			if rnd == 3:
				#U-G
				new_sequence = sequence[0:position_i-1] + 'U' + sequence[position_i:position_j-1] + 'G' + sequence[position_j:]
				mutation = True
			if rnd == 4:
				#G-C
				new_sequence = sequence[0:position_i-1] + 'G' + sequence[position_i:position_j-1] + 'C' + sequence[position_j:]
				mutation = True
			if rnd == 5:
				#C-G
				new_sequence = sequence[0:position_i-1] + 'C' + sequence[position_i:position_j-1] + 'G' + sequence[position_j:]
				mutation = True
			return new_sequence


def mutate_free_nucleotide(sequence, pairs_matrix, position, constrain):
	# returns either a None if nothing new is generated, OR a sequence if a new one is generated
	# if the mutated sequence is the same as the input sequence, None is returned
	if (constrain[position-1] == 'x'):
		return None
	
	mutation = False
	new_sequence = ''
	prob_position_free = pairs_matrix[position-1][len(sequence)]
	prob_mutation = 1 - prob_position_free
	f = random.random()
	
	if f <= prob_mutation:
		nucleotide = random.randint(0,3)
		if nucleotide == 0:
			new_sequence = sequence[0:position-1] + 'A' + sequence[position:]
			mutation = True
		if nucleotide == 1:
			new_sequence = sequence[0:position-1] + 'U' + sequence[position:]
			mutation = True
		if nucleotide == 2:
			new_sequence = sequence[0:position-1] + 'G' + sequence[position:]
			mutation = True
		if nucleotide == 3:
			new_sequence = sequence[0:position-1] + 'C' + sequence[position:]
			mutation = True
	
	if new_sequence == sequence:
		return None
	if mutation == True:
		if new_sequence == sequence:
			return None
		if new_sequence != sequence:
			return new_sequence
	if mutation == False:
		return None


# -- Other methods (i.e. evalution of a sequence, computing ensemble defect, performing non-dominated sorting etc...)
def evaluate_sequence_ensemble_defect_single_objective(id, T, seq, constrain):
	free_energy, ensemble_defect, normalized_ensemble_defect, nupack_matrix, pairs_matrix, nested_pairs_matrix, non_nested_pairs_matrix = compute_ensemble_defect_pseudo(id, T, seq)
	GC_percent = get_gc_percent(seq)
	purine_percent = get_purine_percent(seq)
	pyramydine_percent = get_pyramydine_percent(seq)

	out_dict = {"sequence" : seq,
	"structure" : T,
	"free_energy" : free_energy,
	"ensemble_defect" : ensemble_defect,
	"normalized_ensemble_defect" : normalized_ensemble_defect,
	"nupack_matrix" : nupack_matrix,
	"pairs_matrix" : pairs_matrix,
	"nested_pairs_matrix" : nested_pairs_matrix,
	"non_nested_pairs_matrix" : non_nested_pairs_matrix,
	"GC_percent" : get_gc_percent(seq),
	"purine_percent" : purine_percent,
	"pyramydine_percent" : pyramydine_percent,
	"constrain" : constrain,            
	}
	return out_dict


def evaluate_sequence(id, T, seq, target_GC, constrain, option):
	
	if option == '-pseudo':
		free_energy, ensemble_defect, normalized_ensemble_defect, nupack_matrix, pairs_matrix, nested_pairs_matrix, non_nested_pairs_matrix = compute_ensemble_defect_pseudo(id, T, seq)
		actual_GC = get_gc_percent(seq)
    	GC_defect = abs(TARGET_GC - actual_GC)
    	AVG_defect = float((float(GC_defect) + float(normalized_ensemble_defect))/2)
    	purine_percent = get_purine_percent(seq)
    	pyramydine_percent = get_pyramydine_percent(seq)

    	out_dict = {"option" : option,
    	"sequence" : seq,
        "structure" : T,
        "free_energy" : free_energy,
        "ensemble_defect" : ensemble_defect,
        "normalized_ensemble_defect" : normalized_ensemble_defect,
        "nupack_matrix" : nupack_matrix,
        "pairs_matrix" : pairs_matrix,
        "nested_pairs_matrix" : nested_pairs_matrix,
        "non_nested_pairs_matrix" : non_nested_pairs_matrix,
        "GC_percent" : get_gc_percent(seq),
        "target_GC" : TARGET_GC,
        "GC_defect" : GC_defect,
        "avg_defect" : AVG_defect,
        "purine_percent" : purine_percent,
        "pyramydine_percent" : pyramydine_percent,
        "constrain" : constrain,            
    	}
    	return out_dict
    
	if option == 'no-pseudo':
		free_energy, ensemble_defect, normalized_ensemble_defect, nupack_matrix, pairs_matrix =  compute_ensemble_defect_no_pseudo(T, seq)
		actual_GC = get_gc_percent(seq)
		GC_defect = abs(TARGET_GC - actual_GC)
		AVG_defect = float((float(GC_defect) + float(normalized_ensemble_defect))/2)
		purine_percent = get_purine_percent(seq)
		pyramydine_percent = get_pyramydine_percent(seq)
		out_dict = {"option" : option,
		"sequence" : seq,
		"structure" : T,
		"free_energy" : free_energy,
		"ensemble_defect" : ensemble_defect,
		"normalized_ensemble_defect" : normalized_ensemble_defect,
		"nupack_matrix" : nupack_matrix,
		"pairs_matrix" : pairs_matrix,
		"GC_percent" : get_gc_percent(seq),
		"target_GC" : TARGET_GC,
		"GC_defect" : GC_defect,
		"avg_defect" : AVG_defect,
		"purine_percent" : purine_percent,
		"pyramydine_percent" : pyramydine_percent,
		"constrain" : constrain,
		}
		return out_dict


def hamming_distance(seq1, seq2):
	if seq1 == None or seq2 == None:
		return 0
	dist = 0
	for i in xrange(0, len(seq1)):
		if seq1[i] != seq2[i]:
			dist = dist + 1
	return dist


def compute_ensemble_defect_no_pseudo(T, seq):
	# returns 3 things ! look at the return statement
	free_energy, nupack_matrix, pairs_matrix = get_matrix_bp_prob_no_pseudo(T, seq)
	structure_matrix =  get_structure_matrix(T)

	N = len(seq)
	SUM = 0
	
	for i in xrange(N):
		for j in xrange(N+1):
			SUM = SUM + (pairs_matrix[i][j] * structure_matrix[i][j])

	ensemble_defect = N - SUM

	normalized_ensemble_defect = ensemble_defect/N

	return free_energy, ensemble_defect, normalized_ensemble_defect, nupack_matrix, pairs_matrix


def compute_ensemble_defect_pseudo(id, T, seq):
	# returns 5 things ! look at the return statement
	free_energy, nupack_matrix, pairs_matrix, matrix_nested_pairs, matrix_non_nested_pairs = get_matrix_bp_prob_pseudo(id, T, seq)
	structure_matrix =  get_structure_matrix(T)

	N = len(seq)
	SUM = 0
	
	for i in xrange(N):
		for j in xrange(N+1):
			SUM = SUM + (pairs_matrix[i][j] * structure_matrix[i][j])

	ensemble_defect = N - SUM

	normalized_ensemble_defect = ensemble_defect/N

	return free_energy, ensemble_defect, normalized_ensemble_defect, nupack_matrix, pairs_matrix, matrix_nested_pairs, matrix_non_nested_pairs


def get_matrix_bp_prob_pseudo(id, T, seq):
	id = id[-11:]
	address = './temp/' + id + '_nupack_prefix_temp.in'
	file = open(address, 'w')
	file.write(seq)
	file.write('\n')
	file.write(T)
	file.close()

	cmd = NUPACK + "/bin/pairs -pseudo -dangles " + DANGLES + " -material " + MATERIAL + TEMPERATURE + " ./temp/" + id + "_nupack_prefix_temp"
	os.popen(cmd)

	#load the content of the output of NUPACK call
	address_2 = './temp/' + id + '_nupack_prefix_temp.ppairs'
	file = open(address_2,'r')
	line = file.readline()

	matrix = [] 
	count = 0
    
	while count < 16:
		if count == 13:
			data = line.split()
			free_energy = data[3]
		line = file.readline()
		count = count + 1
		continue
	while line:
		data = line.split()
		matrix.append(data)
		line = file.readline()
	file.close()

	removal_command_1 = 'rm ' + address
	removal_command_2 = 'rm ' + address_2

	os.popen(removal_command_1)
	os.popen(removal_command_2)

	# clear the content and create an actual 2x2 matrix of basepair probabilities of non pseudoknot pairs
	matrix_pairs = [[0 for x in xrange(len(seq)+1)] for x in xrange(len(seq))]
	matrix_nested_pairs = [[0 for x in xrange(len(seq)+1)] for x in xrange(len(seq))]
	matrix_non_nested_pairs = [[0 for x in xrange(len(seq)+1)] for x in xrange(len(seq))]

	for row in matrix:
		i = int(row[0])
		j = int(row[1])
		p = float(row[2])
		matrix_pairs[i-1][j-1] = p
		if j <= len(seq):
			matrix_pairs[j-1][i-1] = p

	for row in matrix:
		i = int(row[0])
		j = int(row[1])
		p = float(row[3])
		matrix_nested_pairs[i-1][j-1] = p
		if j <= len(seq):
			matrix_nested_pairs[j-1][i-1] = p

	for row in matrix:
		i = int(row[0])
		j = int(row[1])
		p = float(row[4])
		matrix_non_nested_pairs[i-1][j-1] = p
		if j <= len(seq):
			matrix_non_nested_pairs[j-1][i-1] = p

	# matrices have N rows and N+1 columns
	return free_energy, matrix, matrix_pairs, matrix_nested_pairs, matrix_non_nested_pairs


def get_matrix_bp_prob_no_pseudo(T, seq):
    file = open("./temp/nupack_prefix_temp.in", 'w')
    file.write(seq)
    file.write('\n')
    file.write(T)
    file.close()

    cmd = NUPACK + "/bin/pairs -dangles " + DANGLES + " -material " + MATERIAL + TEMPERATURE + " ./temp/nupack_prefix_temp"
    os.popen(cmd)

    #load the content of the output of NUPACK call
    file = open('./temp/nupack_prefix_temp.ppairs','r')
    line = file.readline()

    # to store the output of nupack
    matrix = [] 
    count = 0

    while count < 15:
    	if count == 12:
        	data = line.split()
        	free_energy = data[3]
        line = file.readline()
        count = count + 1
        continue
    while line:
        data = line.split()
        matrix.append(data)
        line = file.readline()
    
    file.close()

    os.popen('rm ./temp/nupack_prefix_temp.in')
    os.popen('rm ./temp/nupack_prefix_temp.ppairs')

    # clear the content and create an actual 2x2 matrix of basepair probabilities of non pseudoknot pairs
    matrix_pairs = [[0 for x in xrange(len(seq)+1)] for x in xrange(len(seq))]

    for row in matrix:
    	i = int(row[0])
    	j = int(row[1])
    	p = float(row[2])
    	matrix_pairs[i-1][j-1] = p
    	if j <= len(seq):
    		matrix_pairs[j-1][i-1] = p

    return free_energy,matrix, matrix_pairs


def get_complementary_bp_pseudo(T, p):
    # covering three different pair of brackets: () , {}, []
    count = 0
    p = int(p) - 1
    if T[p] == '(':
        for i in range(p+1, len(T)+1):
            if T[i] == '(':
                count = count + 1
            if T[i] == ')':
                if count == 0:
                    return i + 1
                else:
                    count = count - 1
    if T[p] == ')':
        for i in range(p, 0, -1):
            #print i, T[i-1]
            if T[i-1] == ')':
                count = count + 1
            if T[i-1] == '(':
                if count == 0:
                    return i
                else:
                    count = count - 1
    if T[p] == '{':
        for i in range(p+1, len(T)+1):
            if T[i] == '{':
                count = count + 1
            if T[i] == '}':
                if count == 0:
                    return i + 1
                else:
                    count = count - 1
    if T[p] == '}':
        for i in range(p, 0, -1):
            #print i, T[i-1]
            if T[i-1] == '}':
                count = count + 1
            if T[i-1] == '{':
                if count == 0:
                    return i
                else:
                    count = count - 1

    if T[p] == '[':
        for i in range(p+1, len(T)+1):
            if T[i] == '[':
                count = count + 1
            if T[i] == ']':
                if count == 0:
                    return i + 1
                else:
                    count = count - 1
    if T[p] == ']':
        for i in range(p, 0, -1):
            #print i, T[i-1]
            if T[i-1] == ']':
                count = count + 1
            if T[i-1] == '[':
                if count == 0:
                    return i
                else:
                    count = count - 1


def get_complementary_bp_no_pseudo(T, p):
    count = 0
    p = int(p) - 1
    if T[p] == '(':
        for i in range(p+1, len(T)+1):
            if T[i] == '(':
                count = count + 1
            if T[i] == ')':
                if count == 0:
                    return i + 1
                else:
                    count = count - 1
    if T[p] == ')':
        for i in range(p, 0, -1):
            #print i, T[i-1]
            if T[i-1] == ')':
                count = count + 1
            if T[i-1] == '(':
                if count == 0:
                    return i
                else:
                    count = count - 1


def get_structure_matrix(T):
	structure_matrix = [[0 for x in xrange(len(T)+1)] for x in xrange(len(T))]
	for base in xrange(len(T)):
		pair = get_complementary_bp_pseudo(T, base+1)
		if pair == None:
			structure_matrix[base][len(T)] = 1
		if pair is not None:
			structure_matrix[base][pair-1] = 1
	return structure_matrix


def get_gc_percent(seq):
	# percentage of bases that are either G or C and is (G + C / A + U + G + C)
	count = 0
	for i in seq:
		if i == 'G' or i == 'C' or i == 'g' or i == 'c':
			count = count + 1
	#print count
	return (count * 100 / float (len(seq))) / 100


def get_purine_percent(seq):
	# base A or G
	count = 0
	for i in seq:
		if i == 'A' or i == 'a' or i == 'G' or i == 'g':
			count = count + 1
	return (count * 100 / float (len(seq))) / 100


def get_pyramydine_percent(seq):
	# base C or U
	count = 0
	for i in seq:
		if i == 'C' or i == 'c' or i == 'U' or i == 'u':
			count = count + 1
	return (count * 100 / float (len(seq))) / 100

def element_in_list(list, element):
	for i in list:
		if i == element:
			return True
	return false	


def generate_seed(T, gc_level, option):
	#gc_percent = gc_level * 100
	seed = ""
	for i in range(1, len(T)):
		if option == '-pseudo':
			complementary = get_complementary_bp_pseudo(T, i+1)
		if option == '-no-pseudo':
			complementary = get_complementary_bp_no_pseudo(T, i+1)
		
		if complementary is None:
			rnd = random.random()
			if rnd < gc_level:
				rnd2 = random.randint(0,1)
				if rnd2 == 0:
					seed = seed[0:i] + 'G' + seed[i+1:]
				if rnd2 == 1:
					seed = seed[0:i] + 'C' + seed[i+1:]
			if rnd >= gc_level:
				rnd2 = random.randint(0,1)
				if rnd2 == 0:
					seed = seed[0:i] + 'A' + seed[i+1:]
				if rnd2 == 1:
					seed = seed[0:i] + 'U' + seed[i+1:]
		
		if complementary is not None:
			complementary = int(complementary)
			rnd = random.random()
			if rnd <= gc_level:
				rnd2 = random.randint(0,1)
				if rnd2 == 0:
					seed = seed[0:i] + 'G' + seed[i+1:]
					seed = seed[0:complementary-1] + 'C' + seed[complementary:]
				if rnd2 == 1:
					seed = seed[0:i] + 'C' + seed[i+1:]
					seed = seed[0:complementary-1] + 'G' + seed[complementary:]
			if rnd > gc_level:
				rnd2 = random.randint(0,3)
				if rnd2 == 0:
					seed = seed[0:i] + 'A' + seed[i+1:]
					seed = seed[0:complementary-1] + 'U' + seed[complementary:]
				if rnd2 == 1:
					seed = seed[0:i] + 'U' + seed[i+1:]
					seed = seed[0:complementary-1] + 'A' + seed[complementary:]
				if rnd2 == 2:
					seed = seed[0:i] + 'G' + seed[i+1:]
					seed = seed[0:complementary-1] + 'U' + seed[complementary:]
				if rnd2 == 3:
					seed = seed[0:i] + 'U' + seed[i+1:]
					seed = seed[0:complementary-1] + 'G' + seed[complementary:]
	return seed


def doesExist(item, list):
	#for i in list:
	#	print i['sequence']
	output = False
	for i in list:
		if i['sequence'] == item['sequence']:
			output = True
			print 'duplicate entry'
			break
	return output


def already_exists(seq, TEMP_POOL):
	yes_no = False
	for item in TEMP_POOL:
		if item['sequence'] == seq:
			yes_no = True
			break
	return yes_no


def update_constrain(constrain, i, j = None):
	if i > len(constrain) or j > len(constrain):
		return constrain
	if j == None:
		constrain = constrain[0:i-1] + 'x' + constrain[i:]
	if j != None:
		if i < j:
			constrain = constrain[0:i-1] + 'x' + constrain[i:j-1] + 'x' + constrain[j:]
		if i > j:
			constrain = constrain[0:j-1] + 'x' + constrain[j:i-1] + 'x' + constrain[i:]
	return constrain


def is_stuck(records, normalized_ensemble_defect):
	#if len(records) < 5:
	#	return False
	#records = records[len(records)-5:]
	#if records[0] == records[1] and records[1] == records[2] and records[2] == records[3] and records[3] == records[4]:
	#	return True

	return False
	if float(normalized_ensemble_defect) > 0.20:
		if len(records) < 21:
			return False
		records = records[len(records)-21:]
		if records[0] == records[1] and records[1] == records[2] and records[2] == records[3] and records[3] == records[4] and records[4] == records[5] and records[5] == records[6] and records[6] == records[7] and records[7] == records[8] and records[8] == records[9] and records[9] == records[10] and records[10] == records[11] and records[11] == records[12] and records[12] == records[13] and records[13] == records[14] and records[14] == records[15] and records[15] == records[16] and records[16] == records[17] and records[17] == records[18] and records[18] == records[19] and records[19] == records[20]:
			return True
		else:
			return False
	if float(normalized_ensemble_defect) <= 0.20 and float(normalized_ensemble_defect) > 0.10:
		if len(records) <= 75:
			return False
		records = records[len(records)-75:]
		if is_all_equal(records) is True:
			return True
		else:
			return False
	if float(normalized_ensemble_defect) <= 0.10 and float(normalized_ensemble_defect) > 0.01:
		if len(records) <= 150:
			return False
		records = records[len(records)-150:]
		if is_all_equal(records) is True:
			return True
		else:
			return False
	
	#return False


def is_all_equal(records):
	for i in xrange(0, len(records)-1):
		if records[i] != records[i+1]:
			return False
	return True


def compare_dicts(list):
	#input is a list of dictionaries. each dictionary constains a sequence, structure and all other relevant information
	# purpose: in a list of dictironaries, return the dictionary with lowest [normalized] ensemble defect
	min_defect = 1.00
	candidate = {}
	pos = 0
	index = 0
	for i in list:
		#print i['normalized_ensemble_defect']
		if i['normalized_ensemble_defect'] < min_defect:
			min_defect = i['normalized_ensemble_defect']
			candidate = i
			index = pos
		pos += 1
	return candidate, index


def create_output(dict, iterations, address):
	file = open(address, 'w')
	output = dict['sequence'] + ',' + str(dict['normalized_ensemble_defect']) + ',' + str(dict['free_energy']) + ',' + str(dict['GC_percent']) + ',' + str(iterations)
	file.write(output)
	file.close()


def extract_structure(path):
	file = open(path, 'r')
	structure = file.readline()[:-1]
	file.close()
	return structure


def generate_open_constrain(structure):
	cons = ''
	for i in structure:
		cons = cons + 'o'
	return cons


def generate_output_file(id, structure, sequence, normalized_ensemble_defect, free_energy, C_design, C_eval):
	# first, compute the probability

	#create the input file for /prob
	id = id[-11:]
	address = './temp/' + id + '_nupack_prefix_temp_prob.in'
	file = open(address, 'w')
	file.write(sequence)
	file.write('\n')
	file.write(structure)
	file.write('\n')
	file.close()

	cmd = NUPACK + "/bin/prob -pseudo -dangles " + DANGLES + " -material " + MATERIAL + TEMPERATURE + ' ' + address[:-3]
	data = os.popen(cmd)
	data = data.readlines()
	probability = float(data[-1:][0][:-1])
	removal_command = 'rm ' + address
	os.popen(removal_command)

	#print 'probability ', probability

	#create results file like this: sequence, normalized_ensemble_defect, free_energy, probablity, C_design, C_evaluation, Ratio
	address = './RESULTS/' + id + '_enzymer_results.txt'
	file = open(address, 'w')
	file.write(sequence)
	file.write(',')
	file.write(str(normalized_ensemble_defect))
	file.write(',')
	file.write(str(free_energy))
	file.write(',')
	file.write(str(probability))
	file.write(',')
	file.write(str(C_design))
	file.write(',')
	file.write(str(C_eval))
	file.write(',')
	file.write(str(float(C_design/C_eval)))
	file.write('\n')
	file.close()


def init_seed_with_motifs(T, motifs, gc_level, option):
	# motifs argument must have form such as oooooAAAoooooGGoooooooCUoooo where o is a wild card and anything else is A, U, G or C
	constrain = ''
	seed = generate_seed(T, gc_level, option)
	# generate constrain from motifs
	for c in motifs:
		if c != 'o':
			constrain = constrain + 'x'
		if c == 'o':
			constrain = constrain + 'o'
	
	#update seed to include the motifs
	seed_with_motifs = ''
	for i in xrange(0, len(seed)):
		if constrain[i] == 'x':
			seed_with_motifs = seed_with_motifs + motifs[i]
		else:
			seed_with_motifs = seed_with_motifs + seed[i]
	return seed_with_motifs, constrain


def pseudoknot_recognized():
	file = open('./temp/temp_prob.in', 'w')
	a_seq = ''
	for i in structure:
		a_seq += 'A'
	file.write(a_seq)
	file.write('\n')
	file.write(structure)
	file.close()
	cmd = NUPACK + "/bin/prob -pseudo -dangles " + DANGLES + " -material " + MATERIAL + TEMPERATURE + ' ' + './temp/temp_prob > ./temp/temp_prob_out.out'
	os.popen(cmd)
	file.close()
	file = open('./temp/temp_prob_out.out', 'r')
	r = file.read()
	r = r.split()
	file.close()

	os.popen('rm ./temp/temp_prob_out.out ./temp/temp_prob.in')

	if "Unclassified" in r:
		return False
	else:
		return True

# -- if there is no pseudoknot, just call NUPACK design --
def design_with_nupack():
	#build the temporary input file for NUPACK design
	temp_file = open('./temp/struct.fold', 'w')
	temp_file.write(structure)
	temp_file.close()

	cmd = cmd = NUPACK + "/bin/design -dangles " + DANGLES + " -material " + MATERIAL + " -fstop " + str(target_normalized_ensemble_defect) + ' ' + TEMPERATURE + " ./temp/struct"
	os.popen(cmd)

	results = open('./temp/struct.summary', 'r')
	data = results.read().split()
	sequence =  data[len(data)-1]
	ensemble_defect = float(data[len(data)-2])
	normalized_ensemble_defect = ensemble_defect/len(sequence)
	free_energy = data[-23]
	GC = data[-32]

	os.popen('rm ./temp/struct.summary ./temp/struct.fold')

	return sequence,normalized_ensemble_defect,free_energy,GC


# if __name__ == '__main__':
# 	main()
