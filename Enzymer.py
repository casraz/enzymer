import json
import os
import argparse
from argparse import RawTextHelpFormatter
import simulation

def set_nupack_env_variable():
	os.putenv("NUPACKHOME", "./nupack3.0")

def load_params():
	# initialize help content
	help_file = open('./how_to.txt', 'r').read()

	argument_parser = argparse.ArgumentParser(description=help_file, formatter_class=RawTextHelpFormatter)
	argument_parser.add_argument('--input', help='requiring the json file containing the design parameters')
	arguments = argument_parser.parse_args()
	json_parameters = json.load(open(arguments.input))
	return arguments.input, json_parameters

def main():
	set_nupack_env_variable()
	input_file_name, input_parameteres = load_params()
	simulation.action(input_file_name, input_parameteres)

if __name__ == '__main__':
	main()