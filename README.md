## Summary

Enzymer is a program for designing RNA sequences with targetted secondary structure including tertiary contacts called pseudoknots.
Enzymer implements an efficient adaptive sampling optimization stratrgy to generate RNA's with low ensemble defect.
Enzymer is written in Python 2.7 and tested on Ubuntu OS. Enzymer uses NUPACK to characterize RNA sequences and their structural ensemble.
If the target structure includes pseudoknots, Enzymer will use Nupack 3.2.2 to envoke the partition function computation.
If the target structure does not include pseudoknots, Enzymer will simply call the "design" function from Nupack 3.0.6.
Please refer to www.nupack.org for user manual and terms of use related to the NUPACK package.

Best way to use Enzymer is by running it inside a Docker container. Simply pull the docker image and run it on any OS without requiring any further configurtations.
Find usage instructions at https://hub.docker.com/r/casra/enzymer

If you find this software useful, please cite:

```
@article{zandi2016adaptive,
  title={An adaptive defect weighted sampling algorithm to design pseudoknotted RNA secondary structures},
  author={Zandi, Kasra and Butler, Gregory and Kharma, Nawwaf},
  journal={Frontiers in genetics},
  volume={7},
  pages={129},
  year={2016},
  publisher={Frontiers}
}
```
and

```
@incollection{najeh2021computer,
  title={Computer-Aided Design of Active Pseudoknotted Hammerhead Ribozymes},
  author={Najeh, Sabrine and Zandi, Kasra and Djerroud, Samia and Kharma, Nawwaf and Perreault, Jonathan},
  booktitle={Ribozymes},
  pages={91--111},
  year={2021},
  publisher={Springer}
}
```

This software is developed by Kasra Zandi and is provided as is; use at your own risk for academic and non-commercial purposes.
For commercial usage contact kasra.zandi@gmail.com
